//
//  FastPairTests.swift
//  FastPairTests
//
//  Created by Andrew Hill on 11/21/16.
//  Copyright © 2016 Andrew Hill. All rights reserved.
//

import XCTest
import CoreGraphics
@testable import FastPair



class FastPairTests: XCTestCase {
    
  override func setUp() {
      super.setUp()
      // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
      // Put teardown code here. This method is called after the invocation of each test method in the class.
      super.tearDown()
  }
  
  func testPointSet() {
    let points = PointSet(n: 10)
    XCTAssert(points.count == 10)
  }
  
  func testDistance() {
    let dist = Distance()
    let point1 = CGPoint(x: CGFloat(10.0), y: CGFloat(10.0))
    let point2 = CGPoint(x: CGFloat(11.0), y: CGFloat(11.0))
    let distance = dist.euclidean(p1: point1, p2: point2)
    print(String(format: "%.8f", distance))
    XCTAssert(String(format: "%.8f", distance) == "1.41421356")
  }
  
  func testInit() {
    let fp = FastPair()
    XCTAssert(fp.min_points == 10)
    XCTAssert(fp.initialized == false)
    XCTAssert(fp.points.count == fp.count)
    XCTAssert(fp.neighbors.count == 0)
  }
  
  func testBuild() {
    let ps = PointSet()
    let fp = FastPair()
    fp.build(pts: ps)
    XCTAssert(fp.count == ps.count)
    XCTAssert(fp.neighbors.count == ps.count)
    XCTAssert(fp.initialized == true)
  }
  
  func testAdd() {
    var fp = FastPair(min_points: 3)
    let point1 = CGPoint(x: CGFloat(10.0), y: CGFloat(10.0))
    let point2 = CGPoint(x: CGFloat(11.0), y: CGFloat(11.0))
    let point3 = CGPoint(x: CGFloat(12.0), y: CGFloat(12.0))
    fp = fp + point1
    XCTAssert(fp.count == 1)
    fp = fp + point2
    XCTAssert(fp.count == 2)
    fp = fp + point3
  }
  
  func testSubtract() {
    var fp = FastPair(min_points: 3)
    let ps = PointSet()
    fp.build(pts: ps)
    
    fp = fp - ps[0]
    
    print(fp.points.count)
    print(ps.count)
    XCTAssert(fp.points.count == ps.count - 1)
  }
  
  func testCallAndClosestPair() {
    let ps = PointSet()
    let fp = FastPair()
    fp.build(pts: ps)
    let cp = fp.closest_pair()
    let bf = fp.closest_pair_brute_force()
    XCTAssert(bf[0] == cp[0])
    XCTAssert(bf[1] == cp[1])
  }
  
  func testAllClosestPair() {
    let ps = PointSet()
    let fp = FastPair()
    fp.build(pts: ps)
    let cp = fp.closest_pair()
    let bf = fp.closest_pair_brute_force()
    XCTAssert(bf[0] == cp[0])
    XCTAssert(bf[1] == cp[1])
  }
  
  func testFindNeighborAndSdist() {
    let ps = PointSet()
    let fp = FastPair()
    fp.build(pts: ps)
    let rando = PointSet(n:1)[0]
    let neigh = fp.find_neighbor(p: rando, append: false)  // Abusing find_neighbor!
    let dist = fp.dist.euclidean(p1: rando, p2: neigh.neigh)
    XCTAssert(abs(dist - neigh.dist) < 1e-8)
    XCTAssert(fp.count == ps.count)  // Make sure we didn't add a point...
    
    let sd = fp.sdist(p: rando)
    XCTAssert(sd[0].1 == neigh.neigh)
  }
  
  func test_cluster() {
    var ps = PointSet()
    var fp = FastPair()
    fp.build(pts: ps)
    
    for _ in 0...fp.count-2 {
      let ab = fp.closest_pair()
      let abDist = fp.dist.euclidean(p1: ab[0], p2: ab[1])
      
      let c = interact(u: ab[0], v: ab[1])
      fp = fp - ab[1]  // Drop b
      fp = fp - ab[0]
      fp = fp + c
      
      let ef = fp.combinations(array: ps, k: 2).sorted { (fp.dist.euclidean(p1: $0[0], p2: $0[1]) ) < ( fp.dist.euclidean(p1: $1[0], p2: $1[1]) ) }[0]
      let efDist = fp.dist.euclidean(p1: ef[0], p2: ef[1])
      
      let g = interact(u: ef[0], v: ef[1])
      
      XCTAssert(abs(abDist - efDist) < 1e-8)
      
      for (idx, objectToCompare) in ps.enumerated() {
        if ef[1] == objectToCompare {
          ps.remove(at: idx)
        }
      }
      for (idx, objectToCompare) in ps.enumerated() {
        if ef[0] == objectToCompare {
          ps.remove(at: idx)
        }
      }
      ps.append(g)
      
      XCTAssert( (ab[0] == ef[0] || ab[1] == ef[0]) && (ab[0] == ef[1] || ab[1] == ef[1]) )
    }
  }
  
  func test_update_point() {
    // Still failing sometimes...
    var ps = PointSet()
    let fp = FastPair()
    fp.build(pts: ps)
    
    let old = ps[1]
    let new = PointSet(n:1)[0]
    
    fp.update_point(old: old, new: new)
    
    var oldInFp = false
    for (_, objectToCompare) in fp.points.enumerated() {
      if old == objectToCompare {
        oldInFp = true
      }
    }
    XCTAssert(!oldInFp)
    
    var newInFp = false
    for (_, objectToCompare) in fp.points.enumerated() {
      if new == objectToCompare {
        newInFp = true
      }
    }
    XCTAssert(newInFp)
    XCTAssert(fp.count == ps.count)
    
    let sd = fp.sdist(p: new)
    if let neigh = fp.neighbors[new.getHash] {
      XCTAssert(sd[0].1 == neigh.neigh)
    } else {
      XCTAssert(false)
    }
  }
  func test_merge_closest() {
    // This needs to be 'fleshed' out more... lots of things to test here
    let ps = PointSet()
    var fp = FastPair()
    fp.build(pts: ps)
    
    var n = ps.count
    while n >= 2 {
      let ab = fp.closest_pair()
      let new = interact(u: ab[0], v: ab[1])
      fp = fp - ab[1]  // Drop b
      fp.update_point(old: ab[0], new: new)
      n = n - 1
    }
    XCTAssert(fp.count == 1)
    XCTAssert(fp.count == n)
  }

}
