//
//  Base.swift
//  FastPair
//
//  Created by Andrew Hill on 11/21/16.
//  Copyright © 2016 Andrew Hill. All rights reserved.
//

import UIKit
import Foundation
import CoreGraphics



public class Distance {
  public func euclidean (p1: CGPoint, p2: CGPoint) -> CGFloat {
    let xDist: CGFloat = (p2.x - p1.x)
    let yDist: CGFloat = (p2.y - p1.y)
    return sqrt((xDist * xDist) + (yDist * yDist))
  }
}

public func PointSet(n: Int = 50, d: Int = 2)  -> Array<CGPoint> {
  // used for creating random point sets
  var result: Array<CGPoint> = []
  for _ in 1...n {
    let rand_x = CGFloat(arc4random_uniform(UInt32(1000)))/1000.0
    let rand_y = CGFloat(arc4random_uniform(UInt32(1000)))/1000.0
    result.append(CGPoint(x: rand_x, y: rand_y))
  }
  return result
}

func interact(u: CGPoint, v: CGPoint) -> CGPoint {
  return CGPoint(
    x: (u.x + v.x) / 2.0,
    y: (u.y + v.y) / 2.0
  )
}

extension CGPoint  {
  public var getHash: Int {
    return x.hashValue << 32 ^ y.hashValue
  }
}

public func ==(lhs: CGPoint, rhs: CGPoint) -> Bool {
  return lhs.equalTo(rhs)
}

//public func !=(lhs: CGPoint, rhs: CGPoint) -> Bool {
//  return !lhs.equalTo(rhs)
//}


class Neighbor {
  var neigh: CGPoint
  var dist: CGFloat
  var orig: CGPoint
  init (orig: CGPoint, neigh: CGPoint, dist: CGFloat = CGFloat(FLT_MAX)) {
    self.neigh = neigh
    self.dist = dist
    self.orig = orig
  }
}

public class FastPair {
  
  let dist = Distance()
  var points: Array<CGPoint> = []
  var min_points: Int
  var initialized: Bool = false
  //var neighbors = [CGPoint: String]()
  var neighbors = Dictionary<Int, Neighbor>()
  
  public init(min_points: Int = 10) {
    // min_points
    //  The minimum number of points to add before initializing the
    //  data-structure. Queries _before_ `min_points` have been added to
    //  the data-structure will be brute-force.
    self.min_points = min_points
  }

  
  static func + (lhs: FastPair, rhs: CGPoint) -> FastPair {
    // Add a point and find its nearest neighbor.
    // There is some extra logic here to allow us to gradually build up the
    // FastPair data-structure until we reach `min_points`. Once `min_points`
    // has been reached, we initialize the data-structure and start to take
    // advantage of the FastPair efficiencies.
    //
    lhs.points.append(rhs)
    if lhs.initialized {
      _ = lhs.find_neighbor(p: rhs)
    } else if (lhs.points.count >= lhs.min_points) {
      lhs.build()
    }
    
    return lhs
  }
  
  static func += ( lhs: inout FastPair, rhs: CGPoint) -> FastPair { return lhs + rhs }
  
  static func - (lhs: FastPair, rhs: CGPoint) -> FastPair {
    // Remove a point and update neighbors.//
    for (idx, objectToCompare) in lhs.points.enumerated() {
      if rhs == objectToCompare {
        lhs.points.remove(at: idx)
      }
    }
    
    if lhs.initialized {
      // We must update neighbors of points for (which `p` had been nearest.
      for q in lhs.points {
        if lhs.neighbors[q.getHash]?.neigh == rhs {
          _ = lhs.find_neighbor(p: q)
        }
      }
    }
    return lhs
  }
  
  
  static func -= ( lhs: inout FastPair, rhs: CGPoint) -> FastPair { return lhs - rhs }
  
  public var count: Int {
    return self.points.count
  }
  
  func build(pts: Array<CGPoint> = []) {
    // Build a FastPairs data-structure from a set of (new) points.
    // Here we use a conga line rather than calling explicitly (re)building
    // the neighbors map multiple times as it is more efficient. This method
    // needs to be called _before_ querying the data-structure or adding/
    // removing any new points. Once it has been called, the internal
    // `initialized` flag will be set to True. Otherwise, simple brute-force
    // versions of queries and calculations will be used.
    if pts.count > 0 {
      self.points += pts
    }
    
    let np = self.count-1
    
    // Go through and find all neighbors, placing then in a conga line
    for i in 0...np-1 {
      // Find neighbor to p[0] to start
      var nbr = i + 1
      var nbd = CGFloat(FLT_MAX)
      
      for j in i+1...np {
        let d = self.dist.euclidean(p1: self.points[i], p2: self.points[j])
        if d < nbd {
          nbr = j
          nbd = d
        }
      }
      
      // Add that edge, move nbr to points[i+1]
      let neigh: Neighbor = Neighbor(orig: self.points[i], neigh: self.points[nbr], dist: nbd)
      self.neighbors[self.points[i].getHash] = neigh
      self.points[nbr] = self.points[i + 1]
      self.points[i + 1] = (self.neighbors[self.points[i].getHash]?.neigh)!

    }
    // No more neighbors, terminate conga line.
    // Last person on the line has no
    let neigh: Neighbor = Neighbor(orig: self.points[np], neigh: self.points[np], dist: CGFloat(FLT_MAX))
    self.neighbors[self.points[np].getHash] = neigh
    
    self.initialized = true
  }
  
  func sdist(p: CGPoint) -> [(CGFloat, CGPoint)] {
    // Compute distances from input to all other points in data-structure.
    //    This returns an iterator over all other points and their distance
    //    from the input point `p`. The resulting iterator returns tuples with
    //    the first item giving the distance, and the second item giving in
    //    neighbor point. The `min` of this iterator is essentially a brute-
    //    force 'nearest-neighbor' calculation. To do this, supply `itemgetter`
    //    (or a lambda version) as the `key` argument to `min`.
    
    return self.points.filter { $0 != p }.map { p2 in return (dist: self.dist.euclidean(p1: p, p2: p2), point: p2) }.sorted { ($0.dist ) < ($1.dist ) }
  }
  
  func closest_pair() -> Array<CGPoint> {
    // Find closest pair by scanning list of nearest neighbors.
    // If `npoints` is less than `min_points`, a brute-force version
    // of the closest pair algrithm is used.
    //
    if self.count < 2 {
      //raise ValueError("Must have `npoints >= 2` to form a pair.")
    } else if self.initialized == false {
        return self.closest_pair_brute_force()
    }
    var a = self.points[0]  // Start with first point
    var d = self.neighbors[a.getHash]?.dist
    for p in self.points {
      if let g = self.neighbors[p.getHash] {
        if g.dist < d! {
          a = p  // Update `a` and distance `d`
          d = g.dist
        }
      }
    }
    let b = self.neighbors[a.getHash]?.neigh
    return [a,b!]
    
  }
  
  
  func update_point(old: CGPoint, new: CGPoint) {
    // Update point location, neighbors, and distances.
    //    All distances to point have changed, we need to recompute all aspects
    //    of the data structure that may be affected. Note that although we
    //    completely recompute the neighbors of the original point (`old`), we
    //    don't explicitly rebuild the neighbors map, since that would double the
    //    number of distance computations made by this routine. Also, like
    //    deletion, we don't change any _other_ point's neighbor to the updated
    //    point.
    self.points.remove(at: self.points.index(of: old)!)
    self.points.append(new)
    
    if self.initialized {
      
      self.neighbors[old.getHash] = nil
      
      let neigh: Neighbor = Neighbor(orig: new, neigh: new)
      
      self.neighbors[new.getHash] = neigh
      
      for q in self.points {
        if q != new {
          let d = self.dist.euclidean(p1: new, p2: q)
          if d < (self.neighbors[new.getHash]?.dist)! {
            let newNeigh: Neighbor = Neighbor(orig: new, neigh: q, dist: d)
            self.neighbors[new.getHash] = newNeigh
          }
          if self.neighbors[q.getHash]?.neigh == old {
            if d > (self.neighbors[q.getHash]?.dist)! {
              _ = self.find_neighbor(p: q)
            } else {
              self.neighbors[q.getHash]?.neigh = new
              self.neighbors[q.getHash]?.dist = d
            }
          }
        }
      }
    }
  }
  
        
        
  func find_neighbor(p: CGPoint, append: Bool = true) -> Neighbor {
    // Find and update nearest neighbor of a given point.//
    // If no neighbors available, set flag for (`_update_point` to find
    if self.count < 2 {
      let neigh: Neighbor = Neighbor(orig: p, neigh: p)
      if (append) {
        self.neighbors[p.getHash] = neigh
      }
      return neigh
    } else {
      // Find first point unequal to `p` itself
      var first_nbr = 0
      if (p == self.points[first_nbr]) {
        first_nbr = 1
      }

      
      let new_neigh = self.points[first_nbr]
      var neigh: Neighbor = Neighbor(orig: p, neigh: new_neigh, dist: self.dist.euclidean(p1: p, p2: new_neigh))
      // Now test whether each other point is closer
      for q in self.points[first_nbr+1..<self.points.endIndex] {
        if p != q {
          let d = self.dist.euclidean(p1: p, p2: q)
          if d < neigh.dist {
            neigh = Neighbor(orig: p, neigh: q, dist: d)
          }
        }
      }
      if (append) {
        self.neighbors[p.getHash] = neigh
      }
      return neigh
    }
  }
  
  public func combinations<CGPoint>(array: Array<CGPoint>, k: Int) -> Array<Array<CGPoint>> {
    if k == 0 {
      return [[]]
    }
    
    if array.isEmpty || array.count == 0 {
      return []
    }
    var array = array
    let head = [array[0]]
    _ = array.remove(at: 0)
    let subcombos = combinations(array: array, k: k - 1)
    var ret = subcombos.map {head + $0}
    ret += combinations(array: array, k: k)
    return ret
  }
  
  public func minimumDistance(array: Array<Array<CGPoint>>) -> Array<CGPoint> {
    var d: CGFloat = CGFloat(FLT_MAX)
    var p: Array<CGPoint> = []
    for points in array {
      let new = self.dist.euclidean(p1: points[0], p2: points[1])
      if (new < d) {
        d = new
        p = points
      }
    }
    return p
  }
  
  public func closest_pair_brute_force() -> Array<CGPoint> {
    return _closest_pair_brute_force(points: self.points)
  }
  
  private func _closest_pair_brute_force(points: Array<CGPoint>) -> Array<CGPoint> {
    /* Compute closest pair of points using brute-force algorithm.
    Notes
    Computes all possible combinations of points and compares their distances.
    This is _not_ efficient, nor scalable, but it provides a useful reference
    for (more efficient algorithms. This version is actually pretty fast due
    to its use of fast Python builtins.
    */
    let combos = combinations(array: points, k: 2)
    return minimumDistance(array: combos)
  }
  
}


//
//class Point {
//  var x : CGFloat
//  var y : CGFloat
//  var precision: Int = 12
//
//  init(point : CGPoint) {
//    self.x = point.x
//    self.y = point.y
//  }
//
//  var hashValue : String {
//    get {
//      let base32: String = "0123456789bcdefghjkmnpqrstuvwxyz"
//      let lat = self.y;
//      let lon = self.x;
//
//      var idx = 0; // index into base32 map
//      var bit = 0; // each char holds 5 bits
//      var evenBit = true;
//      var geohash = "";
//
//      var latMin: CGFloat =  -90, latMax: CGFloat =  90;
//      var lonMin: CGFloat = -180, lonMax: CGFloat = 180;
//
//      while (geohash.characters.count < precision) {
//        if (evenBit) {
//          // bisect E-W longitude
//          let lonMid: CGFloat = (lonMin + lonMax) / 2;
//          if (lon >= lonMid) {
//            idx = idx*2 + 1;
//            lonMin = lonMid;
//          } else {
//            idx = idx*2;
//            lonMax = lonMid;
//          }
//        } else {
//          // bisect N-S latitude
//          let latMid = (latMin + latMax) / 2;
//          if (lat >= latMid) {
//            idx = idx*2 + 1;
//            latMin = latMid;
//          } else {
//            idx = idx*2;
//            latMax = latMid;
//          }
//        }
//        evenBit = !evenBit;
//        bit += 1
//        if (bit == 5) {
//          // 5 bits gives us a character: append it and start over
//          geohash += String(base32[base32.index(base32.startIndex, offsetBy: idx)])
//          bit = 0;
//          idx = 0;
//        }
//      }
//
//      return geohash;
//    }
//  }
//}
//
